const express = require('express');
const router = express.Router();
const compareService = require('../../services/').compareService;

/* POST compare */
router.post('/api/compare', function(req, res, next) {

  /**
   * Check whether we have real data to process
   * */
  if (Object.keys(req.body).length) {
    let
      comparedResult = compareService.getComparedResult(req.body);
    res.render('results', { result: comparedResult });

  } else {
    res.send({});
  }
});

module.exports = router;