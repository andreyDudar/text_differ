/**
 * Store special signs for substitution for verbose names to it
 * */
const STATUS = {
  changed: '*',
  onlyInBase: '-',
  onlyInCompare: '+',
  notChanged: ''
};

class CompareService {
  constructor() {
  }

  /***
   * Split comparing sets by lines and trim extra space
   * */
  splitByLines(str) {
    return str.split('\n').map(s => s.trim());
  }

  /**
   * Get properly formatted sets
   * */
  getCompareSets(rawSet) {
    return {
      base: this.splitByLines(rawSet.base),
      compare: this.splitByLines(rawSet.compare)
    }
  }

  /**
   * Find all matches in the given sets
   * */
  findLinesMatch(base, compare) {
    let
      matches = [],

      /**
       * Subset will contain matched lines
       * */
      subset = {
        a: {
          start: null,
          length: 0,
          value: []
        },
        b: {
          start: null,
          length: 0,
          value: []
        },
        active: false,
      },
      longest = base.length > compare.length ? base : compare;

    for (let i = 0; i < longest.length; i++) {
      /**
       * Remember current index to be able to iterate over it
       * in the inner loop
       * */
      let
        innerI = i;

      for (let j = 0; j < longest.length; j++) {

        if (base[innerI] === compare[j]) {
          subset.a.start = subset.a.start === null ? innerI : subset.a.start;
          subset.b.start = subset.b.start === null ? j : subset.b.start;

          subset.a.length++;
          subset.b.length++;
          subset.a.value.push(base[innerI]);
          subset.b.value.push(base[innerI]);
          subset.active = true;
          innerI++;
        } else {
          if (subset.active) {
            matches.push(subset);
            innerI = i;
            i += subset.a.length - 1;
          }

          /**
           * Reset subset
           * */
          subset = {
            a: {
              start: null,
              length: 0,
              value: []
            },
            b: {
              start: null,
              length: 0,
              value: []
            },
            active: false
          };
        }
      }
    }

    return matches;
  }

  /**
   * Filter unusable matching items,
   * need improvements
   * */
  filterMatchedResult(result) {
    let
      filtered = [];

    for (let i = 0; i < result.length; i++) {
      let
        sample = result[i],
        candidate;

      for (let j = 0; j < result.length; j++) {

        if (j === i) {
          continue;
        }

        candidate = result[j];

        if (sample.a.length > candidate.a.length) {
          sample.priority = true;
        } else {
          candidate.priority = true;
        }

        sample.quality = !((sample.a.start < candidate.a.start && candidate.a.start + candidate.a.length - 1 > sample.b.start + sample.b.length - 1) &&
          (sample.a.start > candidate.a.start + candidate.a.length - 1 && sample.b.start < candidate.b.start));
      }

      /**
       * Add item to the list by quality and priority
       * */
      if (!candidate) {
        filtered.push(sample);
      } else {
        if (sample.priority) {
          filtered.push(sample);
        } else if (candidate.priority) {
          filtered.push(candidate);
        } else if (sample.quality) {
          filtered.push(sample);
        } else if (candidate.quality) {
          filtered.push(candidate);
        }
      }
    }

    /**
     * Clean set to remove duplicates
     * */
    let
      uniqueSet = {};

    filtered.map(el => {
      uniqueSet[el.a.value.join('')] = el;
    });

    filtered = [];

    Object.keys(uniqueSet).map(key => {
      filtered.push(uniqueSet[key])
    });

    return filtered;
  }

  /**
   * Handle sequence of lines
   * */
  putInOrder(input, matched) {
    let
      {base, compare} = input,
      resultSet = [],
      resultLine = 1,
      value,
      /**
       * Find the longest sequence of lines to iterate over it
       * */
      longest = base.length > compare.length ? base : compare,
      matchedIdx = 0;

    /**
     * Handle case if there isn't any matches in the given sequence
     * */
    if (!matched.length) {
      for (let i = 0, j = 0; i < longest.length || j < longest.length; i++, j++) {
        /**
         * If we don't have any matched lines - handle it in special way
         * */
        value = {};

        if (base[i] === compare[j]) {
          value.status = 'changed';
          value.value = `${base[i]} | ${compare[j]}`;
        } else {
          if (base[i]) {
            value.status = 'onlyInBase';
            value.value = base[i];
            j = -1;
          } else {
            value.status = 'onlyInCompare';
            value.value = compare[j];
          }
        }

        value.status = STATUS[value.status];
        value.index = resultLine++;
        resultSet.push(value);
      }
    }

    /**
     * Check for available matching
     * */
    if (matched[matchedIdx]) {
      let
        addMatchedLines = false,
        waitForMatchBlock = {
          i: false,
          j: false
        },
        stateBeforeMatched = {
          i: 0,
          j: 0
        };

      /**
       * i index handle base set,
       * j index handle compare set
       * */
      let
        i = 0,
        j = 0;

      for (; i < longest.length || j < longest.length; i++, j++) {
        let
          baseEl = null,
          compareEl = null;

        /**
         * If we are not handling matched lines, try to use given line's value
         * */
        if (!addMatchedLines) {
          /**
           * In this case we don't need to find next value because we are waiting for handling matched lines
           * */
          if (!waitForMatchBlock.i) {
            /**
             * Check that given index doesn't exceed matched lines
             * */
            if (matched[matchedIdx]) {
              if (i < matched[matchedIdx].a.start && i < matched[matchedIdx].a.start + matched[matchedIdx].a.length) {
                baseEl = {
                  value: base[i],
                  notInMatch: true
                };
              } else {
                baseEl = null;
              }
            } else if (base[i]) {
              baseEl = {
                value: base[i],
                notInMatch: true
              };
            } else {
              baseEl = null;
            }
          }

          /**
           * The same logic as for base set of lines
           * */
          if (!waitForMatchBlock.j) {
            if (matched[matchedIdx]) {
              if (j < matched[matchedIdx].b.start && j < matched[matchedIdx].b.start + matched[matchedIdx].b.length) {
                compareEl = {
                  value: compare[j],
                  notInMatch: true
                };
              } else {
                compareEl = null;
              }
            } else if (compare[j]) {
              compareEl = {
                value: compare[j],
                notInMatch: true
              };
            } else {
              compareEl = null;
            }
          }
        }

        /**
         * In this case we have changed line
         **/
        if ((baseEl && baseEl.notInMatch) && (compareEl && compareEl.notInMatch)) {
          value = {
            status: 'changed',
            value: `${baseEl.value}|${compareEl.value}`
          }
        } else if (baseEl && baseEl.notInMatch && !compareEl) {
          value = {
            status: 'onlyInBase',
            value: baseEl.value
          }
        } else if (!baseEl && compareEl && compareEl.notInMatch) {
          value = {
            status: 'onlyInCompare',
            value: compareEl.value
          }
        }

        /**
         * In this case we already can't find line before matched lines
         * and we need to remember current index of sequence
         * to be able to return to it when we use everything
         * items before matching
         * */
        if (!baseEl && !waitForMatchBlock.i) {
          stateBeforeMatched.i = i--;
          waitForMatchBlock.i = true;
        }

        if (!compareEl && !waitForMatchBlock.j) {
          stateBeforeMatched.j = j--;
          waitForMatchBlock.j = true;
        }

        /**
         * If we didn't find any elements for now -
         * find it in matching sets,
         * otherwise - add found value to the set
         * */
        if (!baseEl && !compareEl && matched[matchedIdx]) {
          /**
           * If we just started to handle matched lines -
           * set indexes to the last indexes before matched lines
           * */
          if (!addMatchedLines) {
            i = stateBeforeMatched.i;
            j = stateBeforeMatched.j;

            /**
             * Otherwise, enter into handling matched lines by set
             * addMatchedLines flag to true
             * */
            addMatchedLines = true;

            /**
             * Handle matching items
             * */
            matched[matchedIdx].a.value.forEach(val => {
              value = {
                status: 'notChanged',
                value: val
              };

              value.status = STATUS[value.status];
              value.index = resultLine++;
              resultSet.push(value);
            });

            /**
             * Set proper flags,
             * set index next to the last index of matched items
             * and increase matched index
             * */
            waitForMatchBlock.i = false;
            waitForMatchBlock.j = false;
            addMatchedLines = false;
            i = matched[matchedIdx].a.start + matched[matchedIdx].a.length - 1;
            j = matched[matchedIdx].b.start + matched[matchedIdx].b.length - 1;

            matchedIdx++;
          }

        } else if (baseEl || compareEl) {
          value.status = STATUS[value.status];
          value.index = resultLine++;
          resultSet.push(value);
        }
      }
    }
    return resultSet;
  }

  /**
   * Main method - it handle all process by himself
   * */
  getComparedResult(rawSet) {
    const input = this.getCompareSets(rawSet);

    let
      linesMatch = this.findLinesMatch(input.base, input.compare),
      filteredResult = this.filterMatchedResult(linesMatch);

    return this.putInOrder(input, filteredResult)
  }
}

module.exports = new CompareService();

